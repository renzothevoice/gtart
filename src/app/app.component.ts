import { MapPage } from './../pages/map/map';
import { ArtMarketPage } from './../pages/art-market/art-market';
import { PickAvatarPage } from './../pages/pick-avatar/pick-avatar';
import { SplashPage } from './../pages/splash/splash';
import { LoginPage } from './../pages/login/login';
import { Component, ViewChild } from '@angular/core';
import { Nav, Platform } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';

import { HomePage } from '../pages/home/home';
import { CollectArtPage } from '../pages/collect-art/collect-art';
import { ThiefPage } from '../pages/thief/thief';
import { FurtoPage } from '../pages/furto/furto';

@Component({
  templateUrl: 'app.html'
})
export class MyApp {
  @ViewChild(Nav) nav: Nav;

  rootPage: any = SplashPage;

  pages: Array<{title: string, component: any}>;

  constructor(public platform: Platform, public statusBar: StatusBar, public splashScreen: SplashScreen) {
    this.initializeApp();

    // used for an example of ngFor and navigation
    this.pages = [
      { title: 'Home', component: HomePage },
      { title: 'Login', component: LoginPage },
      { title: 'Mercato', component: ArtMarketPage },
      { title: 'Mondo', component: MapPage },
      { title: 'Splash', component: SplashPage },
      { title: 'Ladro', component: ThiefPage },
      { title: 'Furto', component: FurtoPage },
      { title: 'Colleziona Opere', component: CollectArtPage }
    ];

  }

  initializeApp() {
    this.platform.ready().then(() => {
      // Okay, so the platform is ready and our plugins are available.
      // Here you can do any higher level native things you might need.
      this.statusBar.styleDefault();
      this.splashScreen.hide();
    });
  }

  openPage(page) {
    // Reset the content nav to have just this page
    // we wouldn't want the back button to show in this scenario
    this.nav.setRoot(page.component);
  }
}
