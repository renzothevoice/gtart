import { CamionPrimaPage } from './../pages/camion-prima/camion-prima';
import { GameMenuPage } from './../pages/game-menu/game-menu';
import { PuzzleGamePage } from './../pages/puzzle-game/puzzle-game';
import { MapPage } from './../pages/map/map';
import { MakeOfferPage } from './../pages/make-offer/make-offer';
import { GainCreditsPage } from './../pages/gain-credits/gain-credits';
import { CollectArtPage } from './../pages/collect-art/collect-art';
import { PickAvatarPage } from './../pages/pick-avatar/pick-avatar';
import { SplashPage } from './../pages/splash/splash';
import { LoginPage } from './../pages/login/login';
import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule, Avatar } from 'ionic-angular';

import { MyApp } from './app.component';
import { HomePage } from '../pages/home/home';


import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { ArtMarketPage } from '../pages/art-market/art-market';
import { RankingPage } from '../pages/ranking/ranking';
import { ThiefPage } from '../pages/thief/thief';
import { FurtoPage } from '../pages/furto/furto';
import { ThiefJpgPage } from '../pages/thief-jpg/thief-jpg';

@NgModule({
  declarations: [
    MyApp,
    HomePage,
    LoginPage,
    SplashPage,
    PickAvatarPage,
    ArtMarketPage,
    CollectArtPage,
    GameMenuPage,
    GainCreditsPage,
    MakeOfferPage,
    MapPage,
    PuzzleGamePage,
    RankingPage,
    ThiefPage,
    FurtoPage,
    CamionPrimaPage,
    ThiefJpgPage
  ],
  imports: [
    BrowserModule,
    IonicModule.forRoot(MyApp),
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    HomePage,
    LoginPage,
    SplashPage,
    PickAvatarPage,
    ArtMarketPage,
    CollectArtPage,
    GainCreditsPage,
    GameMenuPage,
    MakeOfferPage,
    MapPage,
    PuzzleGamePage,
    RankingPage,
    ThiefPage,
    FurtoPage,
    CamionPrimaPage,
    ThiefJpgPage
  ],
  providers: [
    StatusBar,
    SplashScreen,
    {provide: ErrorHandler, useClass: IonicErrorHandler}
  ]
})
export class AppModule {}
