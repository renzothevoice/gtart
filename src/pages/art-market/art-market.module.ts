import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ArtMarketPage } from './art-market';

@NgModule({
  declarations: [
    ArtMarketPage,
  ],
  imports: [
    IonicPageModule.forChild(ArtMarketPage),
  ],
})
export class ArtMarketPageModule {}
