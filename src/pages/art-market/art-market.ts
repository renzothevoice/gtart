import { MakeOfferPage } from './../make-offer/make-offer';
import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

/**
 * Generated class for the ArtMarketPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-art-market',
  templateUrl: 'art-market.html',
})
export class ArtMarketPage {

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ArtMarketPage');
  }

  navigate() {
    this.navCtrl.push(MakeOfferPage);
  }
}
