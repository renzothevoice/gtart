import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { CamionPrimaPage } from './camion-prima';

@NgModule({
  declarations: [
    CamionPrimaPage,
  ],
  imports: [
    IonicPageModule.forChild(CamionPrimaPage),
  ],
})
export class CamionPrimaPageModule {}
