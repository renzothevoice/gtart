import { FurtoPage } from './../furto/furto';
import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

/**
 * Generated class for the CamionPrimaPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-camion-prima',
  templateUrl: 'camion-prima.html',
})
export class CamionPrimaPage {

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad CamionPrimaPage');
  }

  navigate() {
    this.navCtrl.push(FurtoPage);
  }
}
