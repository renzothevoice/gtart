import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { CollectArtPage } from './collect-art';

@NgModule({
  declarations: [
    CollectArtPage,
  ],
  imports: [
    IonicPageModule.forChild(CollectArtPage),
  ],
})
export class CollectArtPageModule {}
