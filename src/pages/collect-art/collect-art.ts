import { ThiefPage } from './../thief/thief';
import { ArtMarketPage } from './../art-market/art-market';
import { MapPage } from './../map/map';
import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

/**
 * Generated class for the CollectArtPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-collect-art',
  templateUrl: 'collect-art.html',
})
export class CollectArtPage {

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad CollectArtPage');
  }

  navigate() {
    this.navCtrl.push(ArtMarketPage);
  }
}
