import { ThiefPage } from './../thief/thief';
import { CollectArtPage } from './../collect-art/collect-art';
import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';

/**
 * Generated class for the FurtoComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: 'furto',
  templateUrl: 'furto.html'
})
export class FurtoPage {

  text: string;

  constructor(public navCtrl: NavController, public navParams: NavParams) {
    console.log('Hello FurtoPage Component');
    this.text = 'Hello World';
  }

  navigate() {
    this.navCtrl.push(ThiefPage);
  }
}
