import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { GainCreditsPage } from './gain-credits';

@NgModule({
  declarations: [
    GainCreditsPage,
  ],
  imports: [
    IonicPageModule.forChild(GainCreditsPage),
  ],
})
export class GainCreditsPageModule {}
