import { CollectArtPage } from './../collect-art/collect-art';
import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

/**
 * Generated class for the GainCreditsPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-gain-credits',
  templateUrl: 'gain-credits.html',
})
export class GainCreditsPage {

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad GainCreditsPage');
  }

  navigate() {
    this.navCtrl.push(CollectArtPage);
  }
}
