import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { PickAvatarPage } from './pick-avatar';

@NgModule({
  declarations: [
    PickAvatarPage,
  ],
  imports: [
    IonicPageModule.forChild(PickAvatarPage),
  ],
})
export class PickAvatarPageModule {}
