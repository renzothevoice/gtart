import { ThiefPage } from './../thief/thief';
import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { ThiefJpgPage } from '../thief-jpg/thief-jpg';

/**
 * Generated class for the PickAvatarPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-pick-avatar',
  templateUrl: 'pick-avatar.html',
})
export class PickAvatarPage {

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad PickAvatarPage');
  }
  navigate() {
    this.navCtrl.push(ThiefJpgPage);
  }
}
