import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { PuzzleGamePage } from './puzzle-game';

@NgModule({
  declarations: [
    PuzzleGamePage,
  ],
  imports: [
    IonicPageModule.forChild(PuzzleGamePage),
  ],
})
export class PuzzleGamePageModule {}
