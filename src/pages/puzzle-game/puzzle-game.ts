import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { RankingPage } from '../ranking/ranking';

/**
 * Generated class for the PuzzleGamePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-puzzle-game',
  templateUrl: 'puzzle-game.html',
})
export class PuzzleGamePage {

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad PuzzleGamePage');
  }

  navigate() {
    this.navCtrl.setRoot(RankingPage);
  }

}
