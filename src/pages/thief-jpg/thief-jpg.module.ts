import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ThiefJpgPage } from './thief-jpg';

@NgModule({
  declarations: [
    ThiefJpgPage,
  ],
  imports: [
    IonicPageModule.forChild(ThiefJpgPage),
  ],
})
export class ThiefJpgPageModule {}
