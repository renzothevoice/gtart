import { CamionPrimaPage } from './../camion-prima/camion-prima';
import { FurtoPage } from './../furto/furto';
import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

/**
 * Generated class for the ThiefJpgPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-thief-jpg',
  templateUrl: 'thief-jpg.html',
})
export class ThiefJpgPage {

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ThiefJpgPage');
  }

  navigate() {
    this.navCtrl.push(CamionPrimaPage);
  }
}
