import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ThiefPage } from './thief';

@NgModule({
  declarations: [
    ThiefPage,
  ],
  imports: [
    IonicPageModule.forChild(ThiefPage),
  ],
})
export class ThiefPageModule {}
