import { ArtMarketPage } from './../art-market/art-market';
import { FurtoPage } from './../furto/furto';
import { GainCreditsPage } from './../gain-credits/gain-credits';
import { CollectArtPage } from './../collect-art/collect-art';
import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

/**
 * Generated class for the ThiefPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-thief',
  templateUrl: 'thief.html',
})
export class ThiefPage {

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }
  
  ionViewDidLoad() {
    console.log('ionViewDidLoad ThiefPage');
  }

  navigate() {
    this.navCtrl.push(CollectArtPage);
  }
}
